package uia.com.inventarios;

public class EspacioAlmacenamirnto extends CategoriaInventario{
    Integer idlugar;

    public EspacioAlmacenamirnto(String idCat, String descCat, String sinEstatus, String cantidad, Integer id) {
        super( idCat, descCat, sinEstatus, cantidad );
        this.idlugar = id;
    }

    public EspacioAlmacenamirnto(Integer id) {
        this.idlugar = id;
    }

    public EspacioAlmacenamirnto() {

    }

    public Integer getIdlugar() {
        return idlugar;
    }

    public void setId(Integer id) {
        this.idlugar = id;
    }
    public String buscaEspacioAlmacenamiento(int id, String name, String descCat, String cantidad, String idPartida, String idSubpartida, String idCat)
    {
        if(this.getItems().get(getIdlugar()) != null)
        {
            if (this.getItems().get(idPartida).getItems().get(idSubpartida) != null)
            {
                if (this.getItems().get(idPartida).getItems().get(idSubpartida).getItems().get(idCat) != null)
                {
                    CategoriaInventario cat = (CategoriaInventario) this.getItems().get(idPartida).getItems().get(idSubpartida).getItems().get(idCat);
                    return cat.getCantidad();
                }
            }
        }
        return "";
    }


}
