package uia.com.inventarios;

import java.io.IOException;

public class ApartadoMaterial extends InfoItem{
    public ApartadoMaterial(String id, String name, String estatus) {
        super( id, name, estatus );
    }
    public ApartadoMaterial(){

    }
    public String espacioItems(int id, String name, String categoria, String cantidad, String idPartida, String idSubpartida, String idCategoria) throws IOException
{
        EspacioAlmacenamirnto espacioAlmacenamirnto= new EspacioAlmacenamirnto();
        String existencia = "";
        NivelInventario reporteInventario = new NivelInventario();
        existencia = reporteInventario.busca(id, name, categoria, cantidad, idPartida, idSubpartida, idCategoria);
        if (espacioAlmacenamirnto.buscaEspacioAlmacenamiento( id, name, categoria, cantidad, idPartida, idSubpartida, idCategoria) != null) {
            return "ok";

        }
        return "";


}


}
